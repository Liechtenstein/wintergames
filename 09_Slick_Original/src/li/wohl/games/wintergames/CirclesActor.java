package li.wohl.games.wintergames;

import org.newdawn.slick.Graphics;

public class CirclesActor {
	private float x,y,radius;
	private float speed;
	
	public CirclesActor(int x, int y, int radius, float speed){
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.speed = speed;
	}
	
	public void update(int millisSinceLastCall){
		this.y =this.y+(speed*millisSinceLastCall);
		if(this.y>600){
			this.y=0;
		}
	}
	
	public void render(Graphics g){
		g.fillOval(this.x, this.y,radius *2, radius*2);
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
}
