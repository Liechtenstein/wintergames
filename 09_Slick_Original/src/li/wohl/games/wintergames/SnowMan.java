package li.wohl.games.wintergames;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class SnowMan {

	private float x,y;
	private Image image;
	
	public SnowMan(float x, float y) throws SlickException{
		super();
		this.x = x;
		this.y = y;
		
		this.image = new Image("testdata/wallpaper/Snowman.png");
	}
	
	public void render(Graphics g){
		this.image.draw(this.x, this.y, 90, 120);
	}
	
	public void moveDown(){
		this.y = this.y + 10;
	}
	
	public void moveUp(){
		this.y = this.y - 10;
	}
}
