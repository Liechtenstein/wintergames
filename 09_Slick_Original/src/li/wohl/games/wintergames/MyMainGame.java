package li.wohl.games.wintergames;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class MyMainGame extends BasicGame {

	private ArrayList<CirclesActor> actors;
	private SnowMan snowman;
	
	public MyMainGame() {
		super("game");
	}
	
	//------------------------------------------------------------
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
	
			for(int i=0; i<this.actors.size();i++){
				actors.get(i).render(g);
			}
			
			this.snowman.render(g);
	}
	
	//------------------------------------------------------------
	@Override
	public void init(GameContainer gc) throws SlickException {
	
		
		this.actors = new ArrayList<>();
			
			//Grosse Schneeflocken, Geschwindigkeit schnell: 
			for(int i=0;i<50;i++){
				Random random=new Random();
				
				int width=random.nextInt(800);
				int height=random.nextInt(600);
				
				this.actors.add(new CirclesActor(width,height,9,0.15f));
			}	
			
			//Mittlere Schneeflocken, Geschwindigkeit mittel: 
			for(int i=0;i<50;i++){
				Random random=new Random();
				
				int width=random.nextInt(800);
				int height=random.nextInt(600);
				
				this.actors.add(new CirclesActor(width,height,6,0.1f));
			}
			//------------------------------------------------------------
			//Kleine Schneeflocken, Geschwindigkeit langsam: 
			for(int i=0;i<50;i++){
				Random random=new Random();
				
				int width=random.nextInt(800);
				int height=random.nextInt(600);
				
				this.actors.add(new CirclesActor(width,height,2,0.06f));
			}
			//------------------------------------------------------------
			
			
			this.snowman = new SnowMan(50,400);
	}
	//------------------------------------------------------------
	
	
	//------------------------------------------------------------
	@Override
	public void update(GameContainer gc, int millisSinceLastCall) throws SlickException {
		for(int i=0; i<this.actors.size();i++){
			actors.get(i).update(millisSinceLastCall);
		}

	}
	//------------------------------------------------------------
	@Override
	public void keyPressed(int key, char c) {
		super.keyPressed(key, c);
		System.out.println(key);
		
		if(key == 200 ||c== 'w'){
			this.snowman.moveUp();
		}
		
		if(key == 208 ||c== 's'){
			this.snowman.moveDown();
		}
		
		
	}
	//------------------------------------------------------------
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MyMainGame());
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}


}
